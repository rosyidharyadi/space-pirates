package id.my.rosyidharyadi.spacepirates;


import java.util.Map;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        Config config = new Config("");
        ApiConnector connector = new ApiConnector(config);

//        ngetest dulu
        try {
//            String apiKey = null;
//            Map<String, String> resp = connector.register("joni69", "GALACTIC");
//            if (Objects.equals(resp.get("success"), "true")) {
//                apiKey = resp.get("message");
//                config.writeToConfig("api_key", apiKey);
//            }
            Map<String, String> agentDetails = connector.getAgentDetail();
            for (String key: agentDetails.keySet()) {
                System.out.println(key + ": " + agentDetails.get(key));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
