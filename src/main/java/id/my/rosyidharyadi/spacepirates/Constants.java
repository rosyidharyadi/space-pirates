package id.my.rosyidharyadi.spacepirates;

public class Constants {
    public static final String API_BASE_URL = "https://api.spacetraders.io/v2/";
    public static final String ENDPOINT_REGISTER = "register";
    public static final String ENDPOINT_AGENT_DETAILS = "my/agent";
}
