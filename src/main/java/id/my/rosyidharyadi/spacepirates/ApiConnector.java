package id.my.rosyidharyadi.spacepirates;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ApiConnector {
    private static final Logger logger = LogManager.getLogger(Config.class);
    private String apiKey;

    ApiConnector(Config config) {
        if (!Objects.equals(config.getProperties("api_key"), "")) {
            this.apiKey = config.getProperties("api_key");
        }
    }

    private JSONObject makeRequest(String endPoint, String method, Map<String, String> params) throws Exception {
        try {
            String stringUri = Constants.API_BASE_URL + endPoint;
            String stringParams = Utilities.toURLString(params);
            HttpRequest.Builder requestBuilder = HttpRequest.newBuilder()
                    .header("Content-Type", "application/json");
            if (this.apiKey != null && !this.apiKey.isEmpty() ) {
                requestBuilder = requestBuilder.header("Authorization", "Bearer " + this.apiKey);
            }
            if (Objects.equals(method, "GET")) {
                requestBuilder = requestBuilder
                        .uri(new URI(stringUri + "?" + stringParams))
                        .GET();
            } else if (Objects.equals(method, "POST")) {
                String stringDataJson = (new JSONObject(params)).toString();
                requestBuilder = requestBuilder
                        .uri(new URI(stringUri))
                        .POST(HttpRequest.BodyPublishers.ofString(stringDataJson));
            } else {
                throw new Exception("Invalid method");
            }
            HttpRequest request = requestBuilder.build();

            HttpClient client = HttpClient.newHttpClient();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(response.body());
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    public Map<String, String> register(String agentName, String faction) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("symbol", agentName);
        params.put("faction", faction);
        JSONObject response = makeRequest(Constants.ENDPOINT_REGISTER, "POST", params);
        Map<String, String> registrationResult = new HashMap<>();
        if (response.containsKey("data")) {
            response = (JSONObject) response.get("data");
            registrationResult.put("success", "true");
            String apiKey = (String) response.get("token");
            registrationResult.put("message", apiKey);
            this.apiKey = apiKey;
        } else if (response.containsKey("error")) {
            response = (JSONObject) response.get("error");
            registrationResult.put("success", "false");
            registrationResult.put("message", (String) response.get("message"));
        } else {
            registrationResult.put("success", "false");
            registrationResult.put("message", "At this point I'm not sure what the fuck is going on. Perhaps internet problem");
        }
        return registrationResult;
    }

    public Map<String, String> getAgentDetail() throws Exception {
        Map<String, String> detailResult = new HashMap<>();
        JSONObject response = makeRequest(Constants.ENDPOINT_AGENT_DETAILS, "GET", null);
        JSONObject dataDetail = (JSONObject) response.get("data");
        // TODO: make generalized to-hashmap converter utility, covering all possible val datatypes and nested response
        for (Object key : dataDetail.keySet()) {
            if (dataDetail.get(key).getClass() == Long.class) {
                String val = Long.toString((Long) dataDetail.get(key));
                detailResult.put((String) key, val);
                continue;
            }
            detailResult.put((String) key, (String) dataDetail.get(key));
        }
        return detailResult;
    }
}
