package id.my.rosyidharyadi.spacepirates;

import java.io.*;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Config {
    public String DEFAULT_CONFIG_FILE_PATH = "./spacepirates_user_config";
    public String configFilePath;
    private final File configFile;
    private static final Logger logger = LogManager.getLogger(Config.class);
    private final Properties props;
    public boolean isFirstTime = false;

    Config(String configFilePath) {
        if (configFilePath == null || configFilePath.isEmpty()) {
            this.configFilePath = this.DEFAULT_CONFIG_FILE_PATH;
        } else {
            this.configFilePath = configFilePath;
        }
        props = new Properties();
        this.configFile = new File(this.configFilePath);
        try {
            if (!this.configFile.exists() || this.configFile.isDirectory()) {
                boolean fileCreated = this.configFile.createNewFile();
                if (fileCreated) {
                    String firstTimeMsg = "First time running. Config file created at " + this.configFilePath;
                    System.out.println(firstTimeMsg);
                    logger.info(firstTimeMsg);
                    createConfigTemplate();
                    this.isFirstTime = true;
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        readConfig();
    }

    private void readConfig() {
        try (FileReader reader = new FileReader(this.configFile)) {
            props.load(reader);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void createConfigTemplate() {
        writeToConfig("api_key", "");
    }

    public void writeToConfig(String key, String value) {
        try (FileWriter writer = new FileWriter(this.configFile)) {
            props.setProperty(key, value);
            props.store(writer, "");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public String getProperties(String key) {
        return props.getProperty(key);
    }
}
