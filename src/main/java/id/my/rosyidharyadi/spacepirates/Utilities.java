package id.my.rosyidharyadi.spacepirates;

import org.json.simple.JSONObject;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Utilities {
    public static String toURLString(Map<String, String> params) {
        if (params == null) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        
        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), StandardCharsets.UTF_8));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8));
            result.append("&");
        }
        
        String resultString = result.toString();
        return !resultString.isEmpty()
                ? "?" + resultString.substring(0, resultString.length() - 1)
                : resultString;
    }

    public static Map<String, Object> convertToHashmap(JSONObject jsonObject) {
        // Doing wacky recursive shit
        Map<String, Object> container = new HashMap<>();
        for (Object key : jsonObject.keySet()) {
            Object jsonItemVal = jsonObject.get(key);
            if (jsonItemVal.getClass() != JSONObject.class) { // base case
                container.put((String) key, jsonObject);
            } else {
                container.put((String) key, convertToHashmap((JSONObject) jsonItemVal));
            }
        }
        return container;
    }
}
